package pl.edu.pwsztar.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.edu.pwsztar.exception.MovieNotFoundException;

@ControllerAdvice
class ErrorHandler {
    @ExceptionHandler(MovieNotFoundException.class)
    public ResponseEntity<Void> handleException(MovieNotFoundException e) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
